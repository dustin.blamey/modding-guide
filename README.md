This is the base for a new modding guide for M&B Warband.
I started writting it via Latex, so it can be edited easily without having to start a completely new document.
Everyone can request access to it, so everyone can bring in some bits of knowledge.

If you want to work some bits in, add it simply at GitLab.
You can also download all files into a folder and compile a PDF via your Latex-Software.
Uploading the PDF itself somehow lets the browser crash, not sure why.

For the pictures, don't worry about them. I will them work in by myself piece by piece.
They are mostly for filling the gaps and giving the readers a bit time to breath between huge walls of text.

Thanks for helping ^^